/*
 * Copyright (C) 2003-2014  Pascal Essiembre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.essiembre.eclipse.rbe.model.workbench;


import de.santiv.rbesort.enums.Config;

/**
 * Application preferences, relevant to the resource bundle editor plugin.
 * @author Pascal Essiembre
 * @autor Sven Drees
 */
public final class RBEPreferences {
    /** New Line Type: UNIX. */
    public static final int NEW_LINE_UNIX = 0;
    /** New Line Type: Windows. */
    public static final int NEW_LINE_WIN = 1;
    /** New Line Type: Mac. */
    public static final int NEW_LINE_MAC = 2;

    /**
     * Configurable by GUI
     **/

    public static boolean getConvertEncodedToUnicode() {
        return Config.loadBooleanValue(Config.CONVERT_ENCODED_TO_UNICODE);
    }

    public static boolean getConvertUnicodeToEncoded() {
        return Config.loadBooleanValue(Config.CONVERT_UNICODE_TO_ENCODED);
    }

    public static boolean getKeepEmptyFields() {
        return Config.loadBooleanValue(Config.KEEP_EMPTY_FIELDS);
    }

    public static boolean getGroupKeys() {
        return Config.loadBooleanValue(Config.GROUP_KEYS);
    }

    public static boolean getConvertUnicodeToEncodedUpper() {
        return Config.loadBooleanValue(Config.CONVERT_UNICODE_TO_ENCODED_UPPER);
    }

    public static String getKeyGroupSeparator() {
        return Config.loadValue(Config.KEY_GROUP_SEPARATOR);
    }

    public static int getGroupLevelDepth() {
        return Config.loadIntValue(Config.GROUP_LEVEL_DEPTH);
    }

    public static boolean getAlignEqualSigns() {
        return Config.loadBooleanValue(Config.ALIGN_EQUAL_SIGNS);
    }

    public static boolean getGroupAlignEqualSigns() {
        return Config.loadBooleanValue(Config.GROUP_ALIGN_EQUAL_SIGNS);
    }

    /**
     * Gets whether new lines are escaped or printed as is when generating file.
     * @return <code>true</code> if printed as is.
     */
    public static boolean getNewLineNice() {
        return Config.loadBooleanValue(Config.WRAP_LINE_AT_ESCAPED_NEW_LINE);
    }

    /*****************************************************************/

    public static int getGroupLineBreaks() {
        return 1;
    }

    public static boolean getForceNewLineType() {
        return false;
    }

    public static int getNewLineType() {
        return NEW_LINE_UNIX;
    }


    /**
     * Gets whether lines should be wrapped if too big when generating file.
     * @return <code>true</code> if wrapped
     */
    public static boolean getWrapLines() {
        return false;
    }

    /**
     * Gets the number of character after which lines should be wrapped when
     * generating file.
     * @return number of characters
     */
    public static int getWrapCharLimit() {
        return 80;
    }

    public static boolean getSpacesAroundEqualSigns() {
        return true;
    }

    public static boolean getWrapAlignEqualSigns() {
        return true;
    }

    /**
     * Gets the number of spaces to use for indentation of wrapped lines when
     * generating file.
     * @return number of spaces
     */
    public static int getWrapIndentSpaces() {
        return 1;
    }




}
