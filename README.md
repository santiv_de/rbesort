#RBESort#

**RBESort** is a IntelliJ plugin which helps to sort your property files in the same way like the  fantastic *ResourceBundle Editor* by Paul Essiembre for Eclipse (http://essiembre.github.io/eclipse-rbe/) it does.
     
This plugin should help to avoid merge conflicts if developers of the same project working with Eclipse and IntelliJ.

## Usage ##
Press CTRL+ALT+F to sort your property file.